declare const module: any

import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify'
//@ts-ignore
import * as compression from 'compression'
import * as fs from 'fs'
import * as path from 'path'
import * as dotenv from 'dotenv'

const config = dotenv.parse(fs.readFileSync(`env/${process.env.NODE_ENV}.env`))

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({
      ignoreTrailingSlash: true,
      http2: false,
      https: {
        allowHTTP1: true, // fallback support for HTTP1
        // TODO: setup for production in future
        key: fs.readFileSync(
          path.join(__dirname, '..', 'keys', 'example.com.key'),
        ),
        cert: fs.readFileSync(
          path.join(__dirname, '..', 'keys', 'example.com.crt'),
        ),
      },
    }),
  )

  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }

  app.use(compression())
  await app.listen(config.PORT, config.HOST)
}
bootstrap()
