import { Module } from '@nestjs/common'
import { ConfigService } from '../../services/config/config.service'

@Module({
  providers: [
    {
      provide: ConfigService,
      useValue: new ConfigService(),
    },
  ],
})
export class ConfigModule {}
