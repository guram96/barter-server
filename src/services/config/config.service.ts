import { Injectable } from '@nestjs/common'
import * as dotenv from 'dotenv'
import * as fs from 'fs'
import * as Joi from 'joi'

/**
 * config typedef
 *
 * @export
 * @interface IEnvConfig
 */
export interface IEnvConfig {
  [key: string]: string
}

/**
 * Manage environment configuration
 *
 * @export
 * @class ConfigService
 */
@Injectable()
export class ConfigService {
  /**
   * @private
   * @type {{ [key: string]: string }}
   * @memberof ConfigService
   */
  private readonly envConfig: { [key: string]: string }

  constructor() {
    const config = dotenv.parse(fs.readFileSync(`${process.env.NODE_ENV}.env`))
    this.envConfig = this.validateConfig(config)
  }

  /**
   *
   * get configuration by key
   * @param {string} key
   * @returns {string}
   * @memberof ConfigService
   */
  get(key: string): string {
    return this.envConfig[key]
  }

  /**
   *
   * get node env
   * @readonly
   * @type {Boolean}
   * @memberof ConfigService
   */
  get nodeEnv(): Boolean {
    return Boolean(this.envConfig.NODE_ENV)
  }

  /**
   *
   * get server host
   * @readonly
   * @type {string}
   * @memberof ConfigService
   */
  get host(): string {
    return this.envConfig.HOST
  }

  /**
   *
   * get port
   * @readonly
   * @type {string}
   * @memberof ConfigService
   */
  get port(): string {
    return this.envConfig.PORT
  }

  /**
   *
   * validate configuration
   * @param {IEnvConfig} config
   * @returns {IEnvConfig}
   * @memberof ConfigService
   */
  validateConfig(config: IEnvConfig): IEnvConfig {
    const envVarSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production'])
        .default('development'),
      HOST: Joi.string().default('localhost'),
      PORT: Joi.number().default(3000),
    })

    const { error, value: validateConfig } = Joi.validate(config, envVarSchema)

    if (error) throw new Error(`Config validation error: ${error.message}`)

    return validateConfig
  }
}
